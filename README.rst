==============
Grundlagen UML
==============

- Eine kleine Fallsammlung.
- Konzipiert als Kurs und Nachschlagewerk.

Die aktuelle Build-Version ist direkt abrufbar unter:

https://oer-kurse.gitlab.io/uml/

Peter Koppatz (2022)
