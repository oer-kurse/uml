=================================
Interaktions-Übersichts-Diagramme
=================================




Beispiel: Verkauf
-----------------

Die dargestellten Aktivitätsdiagramme enthalten nur den Namen.
In der linken oberen Ecke findet sich die Abkürzung „ref“. 
Die Details sind in dieser Ansicht uninteressant und können an
anderer Stelle referenziert werden. 

Es können in der Übersicht alle Elemente eines Aktivitätsdiagrammes
verwendet werden.


   .. image:: ./images/interaktion.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" *> <div class="togglehint"> <pre> /* java -jar plantuml.jar -t svg quelle.txt

    @startuml
    start
    repeat
      partition ref {
      #palegreen:Suche Eintrag;
    }
    repeatwhile (Gefunden?)
      partition ref {
        #palegreen:Ausgabe Sucheintrag;
    }
    if (Kaufabschluß?) then (nein) 
      partition ref {
        #orange:Abbruch der\n  Bestellung;
      }
      stop
    else (ja)
      partition ref {
      #palegreen:Neuer Datensatz;
      }
      stop
    endif
    @enduml

    </pre>
    </div>
    </div>

Denkpause
---------


.. image:: ./images/kueche-sauber-genosse.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/kueche-sauber-genosse.jpg' 
                alt='Flotter Spruch' width='400px' />
       </div>
