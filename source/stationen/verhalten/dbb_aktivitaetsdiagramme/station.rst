==================
Aktivitätsdiagramm
==================


.. _20240313T115634:
.. INDEX:: Aktivitätsdiagramm 

- zeigen die Reihenfolge der Abfolge von Aktionen

- enthalten auch Objekte, die im Zusammenhang mit der Aktion direkt auftreten

Aufgaben
--------

Zeichnen Sie ein Aktivitätsdiagramm für die Rückgabe von Leergut an einem Automaten.

Beispiel: Start einer Aktivität
-------------------------------


.. image:: ./images/ad-01.svg

Beispiel: Ein Objekt zwischen zwei Aktionen
-------------------------------------------


.. image:: ./images/ad-02.svg

Am Ausgang und am Eingang einer jeden Aktion wird das gleiche Objekt benutzt.   

Beispiel: unterschiedliche Objekte am Ein- bzw. Ausgang
-------------------------------------------------------


.. image:: ./images/ad-03.svg


Über einen Pin kann der Ausgang bzw. Eingabeparamter benannt werden.            

Beispiel: Aktivitätsende
------------------------


.. image:: ./images/ad-04.svg

Beispiel: Datenflußende
-----------------------


.. image:: ./images/ad-05.svg

Beispiel: Entscheidung und Zusammenführung
------------------------------------------


.. image:: ./images/ad-07.svg

Beispiel: Splitting und Syncronisation
--------------------------------------


.. image:: ./images/ad-06.svg

Beispiel: Wiederholung
----------------------


.. image:: ./images/ad-08.svg

Beispiel: Ticketsystem "Trac"
-----------------------------


.. image:: ./images/ad-09.svg

Beispiel: Spagetti kochen
-------------------------


.. image:: ./images/spagetti-kochen.svg

Denkpause:
----------


.. image:: ./images/sonderzug-2019.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/sonderzug-2019.jpg' 
                alt='Sonderzug 2019 Michendorf' width='400px' />
	<p>Sonderzug in Michendorf anno 2019</p>
       </div>
