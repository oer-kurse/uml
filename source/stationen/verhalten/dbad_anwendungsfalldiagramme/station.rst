=======================
Anwendungsfalldiagramme
=======================


Anwendungsfalldiagramme, auch use-case-Diagramme genannt, zeigen das zu
modellierende System aus der Sicht der Anwender.

Es werden die Grenzen festgelegt und die mit dem System agierenden
Akteure. Es sind oft Personen in bestimmten Rollen, es kann sich aber
auch um bestimmte Programme und Komponenten handeln.

Aufgaben
--------

Erstellen Sie ein Anwendungsfalldiagramm für einen Aufgabenbereich in
Ihrer Firma, in dem Sie aktiv sind.

Symbol: Grenzen des Systems
---------------------------

- Die Grenzen werden durch ein Rechteck dargestellt.

- Die Akteure werden außerhalb der Systemgrenzen platziert, die
  Anwendungsfälle im System.

- Verbindungen zwischen Akteuren und System werden mit einfachen Linien
  dargestellt.

Symbol: Akteur
--------------

.. image:: ./images/akteur.svg

Alternative Darstellung
~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/akteur2.png

Symbol: Anwendungsfall
----------------------

.. image:: ./images/anwendungsfall.svg

Beispiel: Generalisierung
-------------------------

.. image:: ./images/generalisierung.svg

Beispiel: Reklamation
---------------------

.. image:: ./images/reklamation.svg

Beispiel:Kardinalität
---------------------

.. image:: ./images/kardinalitaet.svg

Beispiel: include
-----------------

.. image:: ./images/use-case-include.svg

Beispiel: extend
----------------

.. image:: ./images/use-case-extend.svg

Beispiel: Anwendungsfälle für »Hasse-Diagramme«
-----------------------------------------------

.. image:: ./images/mindset-usecase.svg

Beschreibung als Text
---------------------

Ein Bild sagt mehr als tausend Worte, gut strukturierter Text
darf es auch sein. Nachfolgend zwei Beispiele mit der
Beschreibung eines Anwendungsfalles in Text-Form:

Beispiel I
~~~~~~~~~~

Heute macht man das automatisiert, aber in der Lehrlingsausbildung
wird es manchmal noch anders gehandhabt.


.. code:: text


    Anwendungsfall:         Installation der Schulungsräume
    Akteure:                primär: Azubis, 1.LJ
    Vorbedingungen:         freier Schulungsraum
                            intakte Rechner
    Ablauf:                 Rechner einschalten
                              - Benutzer zurücksetzen
                              - Zusatzsoftware installieren
                              - nicht benötigte Software deinstallieren
                              - Raum säubern / Material nachfüllen
                              - Druckerfunktion testen
                              - Informationsblatt für Dozent drucken
    Sonderfall:             wenn 1.LJ abwesend -> 2.LJ
                            wenn 1. und 2.LJ abwesend -> 3.LJ
    Nachbedingungen:        Rechner runterfahren

Beispiel II
~~~~~~~~~~~

`Nach einem Beispiel der UX-Magazin-Seite <http://uxmag.com/articles/the-broken-telephone-game-of-defining-software-and-ui-requirements>`_

.. code:: text


        Anwendungsfall: Installation in den Schulungsräumen

    Y 1.  Akteure: primär: Azubis, 1.LJ
      2.  Vorbedingungen:
      2.1 freier Schulungsraum
      2.2 intakte Rechner
      3.  Arbeitsschritte:
      3.1 Rechner einschalten
      3.2 Benutzer zurücksetzen
      3.3 Zusatzsoftware installieren
      3.4 nicht benötigte Software deinstallieren
      3.5 Raum säubern / Material nachfüllen
      3.6 Druckerfunktion testen
      3.7 Informationsblatt für Dozent drucken
      4.  Rechner runterfahren

    Alternativen

    1.1. wenn 1.LJ abwesend -> 2.LJ
    1.2. wenn 1. und 2.LJ abwesend -> 3.LJ

Denkpause:
----------


.. image:: ./images/schild-wuensch-dir-was.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/schild-wuensch-dir-was.jpg' 
                alt='Schild: Wünsch Dir was' width='400px' />
       </div>
