=========
Werkzeuge
=========

.. INDEX:: Werkzeuge; UML
.. INDEX:: UML; Werkzeuge

.. toctree::
   :maxdepth: 1
   :glob:

   *

Domain Driven Design
--------------------

- https://www.wps.de/modeler/

Online-Visualisierung
---------------------

- `Swimlanes, angelehnt an Sequenzdiagramme <https://swimlanes.io/>`_
- `Flowcharts, angelehnt an Anwendungsfalldiagramme <https://flowchart.fun>`_
- `draw.io <https://app.diagrams.net>`_
- https://mermaid.js.org
- https://github.com/mermaid-js/mermaid-cli
  
  

