===========================
Pikchar -- Railway-Diagramm
===========================


.. _20240313T102342:
.. INDEX:: Railway-Diagramm; Pikchar
.. INDEX:: Pikchar; Railway-Diagramm

.. meta::

   :description lang=de: UML -- Unified Modeling Language -- Werkzeuge
   :keywords: UML, Werzeuge, Tools, Pikchar

Pikchar
-------

- `https://pikchr.org/ <https://pikchr.org/>`_

- `Beispiele <https://pikchr.org/home/doc/trunk/doc/examples.md>`_

- `Online-Editor <https://pikchr.org/home/pikchrshow>`_

Beispiel: Railway-Diagramm
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: ./images/pikchr-and-or-verknuepfung.svg

.. code:: bash


    # gerendert mit: https://pikchr.org/home/pikchrshow    
    # pikchr-Source:


       $linerad = 10px
       $linewid *= 0.5
       $lineh = 0.75

    SE:  box " SELECT " fill gold color black fit
         arrow
    OT:  oval "Attribut" fit
    CM2: oval "," bold fit at OT.ht*1.25 below OT
         arrow from OT.e right $linerad then down even with CM2 then to CM2.e
         line from CM2.w left even with 2*arrowht left of OT.w \
         then up even with OT then to arrowht left of OT.w
         line  from OT.end  right

         line  from OT.end  right 2*arrowht
         line right then down $lineh*.75 then left even with SE.w 
         L1: line left then down $lineh*.5
    # Ebene 2
    FR:  box "FROM" fill gold color black fit \
         same as SE at SE-(0,$lineh*1.25)
         arrow right from L1.end to FR.w
         arrow from FR.e 
    T1:  oval "Tabellenname" fit

    # Ebne 3
        line right then down $lineh*.5 then left even with SE.w 
    L2: line left then down $lineh*.5  
    WR: box "WHERE" fill gold color black fit \
        same as FR at FR-(0,$lineh*1)
         arrow right from L2.end to WR.w
         arrow from WR.e 
    A1: oval "Attribut" fit
        oval "Operator" fit fill gold color black
        oval "Wert" fit

    # Ebne 4
        line right then down $lineh*.5 then left even with SE.w 
    L3: line left then down $lineh*.5  

    AND: box " AND | OR " fill gold color black fit \
         same as WR at WR-(0,$lineh)
         arrow right from L3.end to AND.w
         arrow from AND.e
    A2:  oval "Attribut" fit
         oval "Operator" fit fill gold color black
         oval "Wert" fit
         oval ";" fill gold color black fit
