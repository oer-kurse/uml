========
Plantuml
========


.. \_20240313T102342:

.. meta::

   :description lang=de: UML -- Unified Modeling Language -- Werkzeuge
   :keywords: UML, Werzeuge, Tools, Plantuml

Wenn Sie mit »Plantuml« Diagramme auf dem eigenen Rechener erstellen
wollen, ist das auch möglich. Was zu beachten ist, wird hier gezeigt.

Download
--------

die jar-Datei herunterladen von: `https://plantuml.com/download <https://plantuml.com/download>`_

Grafik speichern
----------------

Aus dem Quellcode kann dann entweder eine Bilddatei im png-Format
bzw. eine SVG-Grafik erzeugt werden.

In einer Textdatei wird gespeichert was Online auf der Website auch
möglich ist. Ein Beispiel von der Website:

.. code:: bash


    @startuml
    Bob -> Alice : hello
    @enduml

Übersetzung
-----------

Wenn der Dreizeiler in /mein-diagramm.txt gespeichert wird, lautet
der Aufruf:

.. code:: bash


    java -jar plantuml.jar -tpng mein-diagramm.txt

Wenne es kein png-Bild, sondern eine svg-Grafik werden soll, dann:

.. code:: bash


    java -jar plantuml.jar -tsvg mein-diagramm.txt

Das Ergebins
------------

.. image:: ./images/mein-diagramm.svg

Linkliste
---------

- Erklärungen: `http://plantuml.com/ <http://plantuml.com/>`_

- Testserver: `http://www.plantuml.com/plantuml/uml <http://www.plantuml.com/plantuml/uml>`_

- Plantuml + Sphinx: `https://pypi.org/project/sphinxcontrib-plantuml/ <https://pypi.org/project/sphinxcontrib-plantuml/>`_

- Eigenen Testserver aufsetzen (Docker und andere Installationsanleitungen):

  `https://docs.gitlab.com/ee/administration/integration/plantuml.html <https://docs.gitlab.com/ee/administration/integration/plantuml.html>`_

- Download des jar-Files für den lokalen Einsatz

  `https://plantuml.com/download <https://plantuml.com/download>`_

- Dokumentation in verschienden Sprachen

  `http://plantuml.com/de/guide <http://plantuml.com/de/guide>`_

- Aus einem Klassendiagramm Quellcode generieren.

  `https://github.com/jupe/puml2code <https://github.com/jupe/puml2code>`_

Platuml -- Python-Code generieren
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- `https://github.com/mingrammer/diagrams <https://github.com/mingrammer/diagrams>`_

- `https://github.com/bafolts/plantcode <https://github.com/bafolts/plantcode>`_
