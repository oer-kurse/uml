======
Gaphor
======


.. _20240313T104052:
.. INDEX:: Werkzeuge; Gaphor 
.. INDEX:: Gaphor; Werkzeuge 

- `Website <https://gaphor.org/download/>`_

- `Dokumentation <https://docs.gaphor.org/en/latest/index.html>`_
