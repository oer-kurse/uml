====================
Beispiel: Tankstelle
====================


Für eine Tankstelle wurden alle Anwendungsfälle modelliert.  Ein
Vergleich aller Lösungsansätze von vier verschiedenen Autoren wird
hier exemplarisch gezeigt ohne eine Wertung vorzunehmen. Es wurde 
eine Skizze angefertigt und der Entwurf dann mit einem UML-Tool
digitalisiert.


.. tab:: 1

   Lösung 1

   .. image:: ./images/tank01.png
   .. image:: ./images/tank02.png

.. tab:: 2

   Lösung 2

   .. image:: ./images/tank03.png
   .. image:: ./images/tank04.png

.. tab:: 3

   Lösung 3

   .. image:: ./images/tank05.png
   .. image:: ./images/tank06.png
   
.. tab:: 4

   Lösung 4

   .. image:: ./images/tank07.png
   .. image:: ./images/tank08.png

Denkpause:
----------


.. image:: ./images/tintling.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/tintling.jpg' 
                alt='Pilze: Tintlinge' width='400px' />
       </div>
