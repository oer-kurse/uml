========================
Beispiel: Datenmigration
========================


.. _migration:

.. sidebar:: Merke

   Wer noch nie einen Fehler gemacht hat,
   hat sich noch nie an etwas neuem versucht.

   (Albert Einstein)



Für die Migration von Lerninhalten der Lernplattform »Sudile« zur
Lernplattform »Moodle« mußte ein ausgeklügelter Workflow entwickelt
werden, weil der direkte HTML- bzw. Scorm-Export nicht akzeptiert
worden ist. Mit einer Ansage, wahlweise Sequenz- oder
Aktivitätsdiagramme zu verwenden, ohne eine systematische
Auseinandersetzung mit dem UML-Standard, sind erste Entwürfe
entstanden, die zwangsläufig noch viele Fehler enthalten.

Im Vergleich zwei Diagrammtypen, die wahlweise mit drow.io und
plantuml erstellt wurden.

In der nachfolgenden ersten Analyse, wurden APK-Diagrammme als
Alternative zu UML-Diagrammen ins Spiel gebracht, die zum Vergleich
ebenfalls gezeigt werden sollen.

.. tab:: 1

   Version I

   .. image:: ./images/johannes.svg

   .. image:: ./images/johannes-epk.svg

.. tab:: 2

   Version II

   .. image:: ./images/lucas.svg
   
   .. image:: ./images/lucas-epk.svg

.. tab:: 3

   Version III

   .. image:: ./images/philipp.svg
   
   .. image:: ./images/philipp-epk.svg

.. tab:: 4

    ersion IV

   .. image:: ./images/phillipp.svg
   
   .. image:: ./images/phillipp-epk.svg

.. tab:: 5

   Version V

   .. image:: ./images/tim.svg
   
   .. image:: ./images/tim-epk.svg

.. tab:: 6

   Version VI

   .. image:: ./images/tomte.svg
   .. image:: ./images/tomte-epk.svg
   
.. tab:: 7

   Version VII

   .. image:: ./images/devin-epk.svg

.. tab:: 8

   Version VIII

   .. image:: ./images/marc-epk.svg

Denkpause und Abkühlung nach diesen harten Fällen :-)
-----------------------------------------------------


.. image:: ./images/potsdam--familie__gruen.avif
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/potsdam--familie__gruen.avif' 
                alt='Mühlstein im Winter 2020' />
       </div>
