==================
Beispiel: Bankraub
==================


.. index:: UML; Beispiel Bankraub
.. INDEX:: Squenzdiagramm; Bankraub
.. INDEX:: Bankraub; Squenzdiagramm

.. _bankraub:


Für einen Bankraub wurde der Ablauf als Sequenzdiagram modelliert. 
Diese Szenarien sind natürlich nicht für den praktischen Einsatz
gedacht. Die Aufgabe diente lediglich als Übung für die Modellierung. 
Für alle angehenden Krimiautoren fünf interessante Vorlagen, inklusive
einem Augenzwinkern.

Viel Spaß beim vergleichen, betrachten, weiter fantasieren....

.. tab:: 1

   Version I

   .. image:: ./images/bankraub01.svg

.. tab:: 2

   Version II

   .. image:: ./images/bankraub02.svg

.. tab:: 3

   Version III

   .. image:: ./images/bankraub03.svg

.. tab:: 4

    ersion IV

   .. image:: ./images/bankraub04.png

.. tab:: 5

   Version V

   .. image:: ./images/bankraub05.png

Denkpause und Abkühlung nach diesen harten Fällen :-)
-----------------------------------------------------


.. image:: ./images/muehlstein-im-winter2020.webp
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/muehlstein-im-winter2020.webp' 
                alt='Mühlstein im Winter 2020' />
       </div>
