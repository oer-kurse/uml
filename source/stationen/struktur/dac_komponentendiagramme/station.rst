====================
Komponentendiagramme
====================


.. index:: UML; Komponentendiagramme

Komponentendiagramme zeigen eine grobe Struktur des Systems, aber auch
die Aufteilung und das Zusammenspiel einzelner Bestandteile.

Aufgabe
-------

In welche Komponenten würden Sie einen Verein aufteilen? Erstellen Sie
ein Komponentediagramm für einen Verein Ihrer Wahl.

Beispiel: Darstellungsymbole für Komponenten
--------------------------------------------

.. image:: ./images/komponenten.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>

    // java -jar plantuml.jar -t svg quelle.txt

    @startuml
    [Erste Komponente]
    [Andere Komponente] as Comp2
    component Comp3
    component [Letzte\nKomponente] as Comp4
    @enduml

    </pre>
    </div>
    </div>

Beispiel: Komponenten verbinden
-------------------------------

- Komponenten werden über Schnittstellen verbunden.

- Hier stellt der Webserver eine Schnittstelle zur Verfügung.

- Die Datenbank-Komponente nutzt die bereitgestellte Schnittstelle
  des Webservers.


.. image:: ./images/komponenten2.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>

    // java -jar plantuml.jar -t svg quelle.txt

    @startuml
    skinparam interface {
    backgroundColor LightGray
    borderColor orange
    }
    skinparam component {
    FontSize 13
    FontName Courier
    BorderColor black
    BackgroundColor gold
    ArrowFontName Impact
    ArrowColor #FF6655
    ArrowFontColor #777777
    }
    () "Data Access" as DA
    DA - [Datenbank-Komponente]
    [Datenbank-Komponente] ..> () HTTP : use
    HTTP - [Web Server\n »Nginx«] 
    @enduml

    </pre>
    </div>
    </div>

Beispiel: Schnittstellen an einem Port
--------------------------------------

.. index:: Beispiele; Port (Komponenten)

.. index:: Port (Komponenten); Beispiele

- Große Komponenten stellen ihre Funktionalität über einen öffentlichen Zugang
  bereit, der Port genannt wird.

- Ein Port ist ein Rechteck über der Grenze eines Diagramm-Symbols.

.. image:: ./images/komponenten3.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>

    // java -jar plantuml.jar -t svg quelle.txt

    @startuml
    skinparam componentstyle uml2
    left to right direction

    component X {
        port " " as x\ :sub:`out`\
    }

    'u for layouting it more nicely, 0) for lollipop
    x\ :sub:`out`\ -u0)- [Y]
    x\ :sub:`out`\ -u0)- [Z]

    @enduml

    </pre>
    </div>
    </div>

Beispiel: REST APIs mit SSL/TLS-Client-Zertifikaten
---------------------------------------------------

.. image:: ./images/zato-komponente.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>

    // java -jar plantuml.jar -t svg quelle.txt


    @startuml

    skinparam node {
    backgroundColor Yellow
    }

    [Client-Applikation] as Client
    [Externe Applikation] as Extern

    node "Zato-Cluster" {
      [Load-Balander] as Balancer
      [diverse Server] as Server
      Balancer --> Server
    }

    Client -left-> Balancer : SSL/TSL\n Client Zertifikat
    Server --> Extern

    @enduml

    </pre>
    </div>
    </div>

Beispiel: Bibliothek
--------------------

.. index:: Beispiele; Komponenten (Bibliothek)

.. index:: Komponenten (Bibliothek); Beispiele

.. image:: ./images/bibliothek.svg

.. raw:: html



    <div>
    Source Plantuml: <input type="checkbox" value="hint" />
    <div class="togglehint">
    <pre>

    // java -jar plantuml.jar -t svg quelle.txt

    @startuml
    title Bibliothek

    skinparam component {
    FontSize 16
    FontName Courier
    BorderColor blue
    BackgroundColor aliceblue
    ArrowFontName Impact
    ArrowColor #FF6655
    ArrowFontColor #777777
    }

    [Erfassen] as B1
    [Ändern] as B2
    [Suche] as B3
    component  Buch
    component [JDBC] as Comp
    component [DB Manager] as DB

    B1 ..>> Buch
    B2 ..>> Buch
    B3 ..>> Buch
    Buch ..>> DB
    DB ..> Comp

    [Erfassen]
    [Erfragen]
    [Ausleihe]

    Erfassen ..>> Ausleihe 
    Erfragen ..>> Ausleihe
    Ausleihe ..>> DB
    @enduml

    </pre>
    </div>
    </div>

Denkpause:
----------


.. image:: ./images/schwerin-der-traum2.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/schwerin-der-traum2.jpg' 
                alt='Schwerin: Nächtliche Erscheinung' width='400px' />
	<p>
	Schweriner Museen: »Nächtliche Erscheinung« ein Gemälde
	(1650) von Jacob Jordaens (1593-1678)
	</p>
       </div>
