====================
Verteilungsdiagramme
====================


.. index:: UML; Verteilungsdiagramme

Verteilungsdiagramme (deployment diagram) zeigen die physikalische
Struktur eines Projektes.

Wichtige Elemente der Verteilungsdiagramme sind Knoten (Nodes), diese
werden mit Stereotypen gekennzeichnet.

Aufgabe
-------

Zeichnen Sie das Verteilungsdiagramm der elektronischen Geräte in ihrem
Haushalt.

Beispiel: Knoten (Nodes)
------------------------

.. image:: ./files/node1.png

Für die
Beschriftung mit Stereotypen gibt es schon eine Reihe von Beispielen:
«cdrom», «cd-rom», «computer», «disk array», «pc», «pc client», «pc
server», «secure», «server», «storage», «unix server», «user pc».

Beispiel: Instanz eines Kontens
-------------------------------

.. image:: ./files/node2.png

Für eine
Knoten-Instanz wird wie in den Objekt-Diagrammen der Name unterstrichen.

Hinter dem Doppelpunkt kann noch der Stereotyp angegeben werden.

Beispiel: Artefakt
------------------

.. image:: ./files/artifakt1.png

Ein Artefakt (artifact) ist ein Begriff für jede Art von Information die von
den Entwicklern eines Systems erstellt, verändert oder benutzt wird. z.
B. Quellcode, Dokumentation, erstellte Programme... Zusammenfassend: Von
einem Menschen geschaffenes Objekt.

Beispiel: drei Komponenten und ein Artefakt
-------------------------------------------

.. image:: ./files/komponente_mit_artifakt.png

Beispiel: Netzwerk
------------------

.. image:: ./files/netzwerk_mit_artifakt.png

Denkpause:
----------

.. image:: ./files/nashorn.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/nashorn.jpg' 
                alt='Schweriner Schloss' width='400px' />
	<p>Schwerin Museum: Gemälde »Rhinozeros« (1749) von Jean-Baptiste Oudry (1686-1755)</p>
       </div>
