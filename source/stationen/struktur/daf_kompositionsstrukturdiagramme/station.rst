=============================
Kompositions-Strukturdiagramm
=============================


.. index:: UML; Kompositions-Strukturdiagramm

Das Kompositions-Strukturdiagramm (composite structure diagram) zeigt
einheitlich die innere Struktur verschiedener UML-Elemente (Klassen,
Anwendungsfälle, Aktivitäten, etc.).

Aufgaben
--------

Informieren Sie sich auf
`http://de.wikipedia.org <http://de.wikipedia.org/>`_ zu diesem Diagrammtyp.

Beispiel: Komponente mit zwei Teilen (Parts)
--------------------------------------------

.. image:: ./images/komm1.png

Beispiel: Auto
--------------

.. image:: ./images/komm2.png

Denkpause:
----------


.. image:: ./images/vertumnus-und-pomona1.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/vertumnus-und-pomona1.jpg' 
                alt='Schweriner Schloss' width='400px' />
	<p>Schwerin Gemälde: »Palastterrasse mit Vertumnus und Pomona« Peeter Gijsels (1621-1690)</p>
       </div>
