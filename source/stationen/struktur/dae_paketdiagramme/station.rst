==============
Paketdiagramme
==============


.. index:: UML; Paketdiagramme

Paketdiagramme geben einem Gesamtsystem eine logische Struktur.

Aufgaben
--------

Erstellen Sie ein Paketdiagramm für Ihr Projekt.

Beispiel: Artikelverwaltung
---------------------------

.. image:: ./files/paket1.png

Denkpause:
----------


.. image:: ./files/barlach-lachende-alte.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/barlach-lachende-alte.jpg' 
                alt='Barlach: Lachende Alte' width='400px' />
	<p>Museum Schwerin: »Lachende Alte« (1937) Ernst Barlach (1870-1938) </p>
       </div>
