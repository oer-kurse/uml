==================
UML: Diagrammtypen
==================

.. index:: UML; Diagramtmypen

Aufgaben
--------

Lernen Sie die Diagrammtypen zu unterscheiden. Prägen Sie sich wichtige Fragestellungen ein.

Klassendiagramme
----------------

- Welche Klassen enthält mein System?

- Welche Beziehungen bestehen zwischen den Klassen?

- Häufig eingesetzter Diagrammtyp.

Objektdiagramm
--------------

- Welche Objekte existieren zu einem bestimmten Zeitpunkt (Schnappschuß eines Klassendiagramms)?

- Zeigt konkrete Objekte und deren Attribut-Werte.

- Gut zur Veranschaulichung an einem konkreten Beispiel, im Unterschied zur abstakten Betrachtung im Klassendiagramm.

Paketdiagramm
-------------

- Wie behalte ich den Überblick über große Klassenmengen?

- Ist ein zusätzliches Strukturierungsmittel.

Kompositionsstrukturdiagramm
----------------------------

- Wie sieht die Struktur einer Klasse, Komponente oder der Teil eines Systems aus?

- Gut geeignet für 'ist-ein'- oder 'hat-ein'-Beziehungen(Aggregation, Komposition).

Komponentendiagramme
--------------------

- Wie werden meine Strukturen zu wiederverwertbaren Gruppen zusammengefaßt?

- Zeigt die Anordnung und das Zusammenspiel einzelner Komponenten eines Systems.

Verteilungsdiagramme
--------------------

- Wie sieht die physikalische Struktur aus?

- Wo werden Server, Datenbanken und andere Hard- und Software platziert?

- Wie werden sie verteilt?

Anwendungsfalldiagramme
-----------------------

- Welche Akteure (in welcher Rolle) agieren mit dem im Modell dargestellten Anwendungsfällen?

- Außensicht auf das System.
  Obwohl eine statische Betrachtungsweise, sind sie den Verhaltensdiagrammen zugeordnet.

- Gut geeignet einem Auftraggeber einen Überblick zu verschaffen, ohne ins Detail gehen zu müssen.

Aktivitätsdiagramm
------------------

- Wie durchlaufen Informationen, Nachrichten, Daten ein System oder Teilsystem.

- Sehr detaillierte Darstellung ähnlich den Progammablaufplänen (PAP) möglich.

Zustandsautomat
---------------

- Welche Zustände und Zustandsübergänge können durch welche Ereignisse auftreten?

Sequenzdiagramm
---------------

- Welche Objekte tauschen wann Nachrichten aus?

- Wie interagieren die Objekte in einer zeitlichen Abfolge?

Kommunikationsdiagramm
----------------------

- Wer kommuniziert mit wem?

- Wer arbeitet mit wem zusammen?

- Keine Details, vermittelt eher einen Überblick.

Timingdiagramm
--------------

- Wann befindet sich ein Teil/ eine Komponente in welchem Zustand?

- Wenn detaillierte zeitliche Darstellung notwendig ist.

Interaktionsübersichtsdiagramm
------------------------------

- Wie  laufen Interaktionen ab?

- Hohe Abstaktion; Details werden durch Sequenz-, Kommunikations- und Timingdiagramme dargestellt.

Denkpause:
----------


.. image:: ./Pictures/gedenktafel-in-kirche.jpg
   :width: 0px

.. raw:: html
       
       <div style='text-align:center;'>
           <img src='../../../_images/gedenktafel-in-kirche.jpg' 
                alt='Schweriner Schloss' width='400px' />
	   <p>Schwerin: Gedenktafel in Kirche</p>
       </div>
