.. UML documentation master file, created by
   sphinx-quickstart on Mon Aug 12 08:21:25 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

   @startuml
   skinparam actorStyle awesome

   Actor Entwickler #palegreen;line:green;line.dashed;text:green

   package Entwurf {
   usecase L #palegreen;line:green;line.dashed;text:green
   usecase M #palegreen;line:green;line.dashed;text:green
   usecase U #palegreen;line:green;line.dashed;text:green

   Entwickler --> (L)
   Entwickler  --> (U)
   Entwickler -down-> (M)
   }
   @enduml


.. meta::

   :description lang=de: OER-Kurs: UML -- Unified Modeling Language https://oer-kurse.gitlab.io/
   :keywords: UML, PlantUML 

Unified Modeling Language
=========================

.. sidebar:: Anlagen

   :ref:`genindex`
   `Interview mit Grady Booch <https://youtu.be/u7WaC429YcU?feature=shared>`_

.. toctree::
   :maxdepth: 1
   :caption: Inhalt:
   :glob:

   stationen/a_begriffe/*
   stationen/b_uml/station
   stationen/d_diagrammtypen/station
   stationen/struktur/index
   stationen/verhalten/index
   stationen/beispiele/index
   stationen/werkzeuge/index
   stationen/design-pattern/index
